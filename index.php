<!DOCTYPE html>
<html>
	<?php include 'recursos/php/dashboard/menu.php'; ?>
	<ul class="sidebar-nav">
		<li class="sidebar-header">Inicio</li>

		<?php
		 if($_SESSION['tipo'] =='Admin'){

		 
		?>
		<li class="sidebar-item ">
			<a data-bs-target="#dashboards" data-bs-toggle="collapse" class="sidebar-link">
				<i class="align-middle me-2 fas fa-fw fa-table"></i> <span class="align-middle">Inventario</span>
			</a>
			<ul id="dashboards" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
				<li class="sidebar-item"><a class="sidebar-link" href="categoria.php">Categoria</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="marca.php">Marca</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="dispositivos.php">Dispositivos</a></li>
			</ul>
		</li>
		<li class="sidebar-item">
			<a data-bs-target="#Pedidos" data-bs-toggle="collapse" class="sidebar-link collapsed">
				<i class="align-middle me-2 fas fa-fw fa-file"></i> <span class="align-middle">Pedidos</span>
			</a>
			<ul id="Pedidos" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-in.html">Tarifas</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-up.html">Pedidos</a></li>
			</ul>
		</li>

		<li class="sidebar-item">
			<a data-bs-target="#registro" data-bs-toggle="collapse" class="sidebar-link collapsed">
				<i class="align-middle me-2 fas fa-fw fa-book"></i> <span class="align-middle">Registros</span>
			</a>
			<ul id="registro" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-in.html">Historial</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-up.html">Graficas</a></li>
			</ul>
		</li>

		<?php
		
		}

		?>
	</	ul>
	<?php include 'recursos/php/dashboard/nav.php'; ?>			
			
	<?php include 'recursos/php/dashboard/pie.php'; ?>
</html>


