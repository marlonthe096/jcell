<!DOCTYPE html>
<html>
	<?php include 'recursos/php/dashboard/menu.php'; ?>
	<ul class="sidebar-nav">
		<li class="sidebar-header">Inicio</li>
		<li class="sidebar-item active">
			<a data-bs-target="#dashboards" data-bs-toggle="collapse" class="sidebar-link">
				<i class="align-middle me-2 fas fa-fw fa-table"></i> <span class="align-middle">Inventario</span>
			</a>
			<ul id="dashboards" class="sidebar-dropdown list-unstyled collapse show" data-bs-parent="#sidebar">
			<li class="sidebar-item"><a class="sidebar-link" href="categoria.php">Categoria</a></li>
				<li class="sidebar-item "><a class="sidebar-link" href="marca.php">Marca</a></li>
				<li class="sidebar-item active"><a class="sidebar-link" href="dispositivos.php">Dispositivos</a></li>	
			</ul>
		</li>
		<li class="sidebar-item">
			<a data-bs-target="#Pedidos" data-bs-toggle="collapse" class="sidebar-link collapsed">
				<i class="align-middle me-2 fas fa-fw fa-file"></i> <span class="align-middle">Pedidos</span>
			</a>
			<ul id="Pedidos" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-in.html">Tarifas</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-up.html">Pedidos</a></li>
			</ul>
		</li>
		<li class="sidebar-item">
			<a data-bs-target="#registro" data-bs-toggle="collapse" class="sidebar-link collapsed">
				<i class="align-middle me-2 fas fa-fw fa-book"></i> <span class="align-middle">Registros</span>
			</a>
			<ul id="registro" class="sidebar-dropdown list-unstyled collapse " data-bs-parent="#sidebar">
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-in.html">Historial</a></li>
				<li class="sidebar-item"><a class="sidebar-link" href="pages-sign-up.html">Graficas</a></li>
			</ul>
		</li>
	</ul>
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,600);

	

		.button-wrapper {
		font-family: 'Open Sans', sans-serif;
		  position: relative;
		  height:100%;
		  text-align:center;
		  position:relative;
		  text-align: center;
		 
		}

		.button-wrapper span.label {
		  position: relative;
		  z-index: 0;
		  display: inline-block;
		  width: 100%;
		  background: #00bfff;
		  cursor: pointer;
		  color: #fff;
		  padding: 10px 0;
		  text-transform:uppercase;
		  font-size:12px;
		}

		
		#imagen1 {
		    display: inline-block;
		    position: absolute;
		    z-index: 1;
		    width: 100%;
		    height: 50px;
		    top: 0;
		    left: 0;
		    opacity: 0;
		    cursor: pointer;
		}
		#imagen2 {
		    display: inline-block;
		    position: absolute;
		    z-index: 1;
		    width: 100%;
		    height: 50px;
		    top: 0;
		    left: 0;
		    opacity: 0;
		    cursor: pointer;
		}
		#imagen3 {
		    display: inline-block;
		    position: absolute;
		    z-index: 1;
		    width: 100%;
		    height: 50px;
		    top: 0;
		    left: 0;
		    opacity: 0;
		    cursor: pointer;
		}
	</style>
	<?php include 'recursos/php/dashboard/nav.php'; ?>			
			<div class="container-fluid">
					<div class="header">
						<h1 class="header-title">
							Dispositivos
						</h1>
						<p class="header-subtitle">Panel de configuracion de dispositivos.</p>
					</div>
					<div class="row">
						<div class="col-xl-6 col-xxl-5 d-flex">
							<div class="w-100">
								<div class="row">
									<div class="col-sm-6">
										<div class="card">
											<a href="categoria.php" style="text-decoration: none;">
												<div class="card-body">
													<div class="row">
														<div class="col mt-0">
															<h5 class="card-title">Categoria</h5>
														</div>

														<div class="col-auto">
															<div class="avatar">
																<div class="avatar-title rounded-circle bg-primary-dark">
																	<i class="align-middle" data-feather="list"></i>
																</div>
															</div>
														</div>
													</div>
													<h1 class="display-5 mt-2 mb-4">1</h1>
													<div class="mb-0">
														Número de Categorias
													</div>
												</div>
											</a>
										</div>
										<div class="card">
											<a href="#" style="text-decoration: none;">
												<div class="card-body" style="background-color: #a8abf180;">
													<div class="row">
														<div class="col mt-0">
															<h5 class="card-title">Dispositivos</h5>
														</div>

														<div class="col-auto">
															<div class="avatar">
																<div class="avatar-title rounded-circle bg-primary-dark">
																	<i class="align-middle" data-feather="package"></i>
																</div>
															</div>
														</div>
													</div>
													<h1 class="display-5 mt-2 mb-4">1</h1>
													<div class="mb-0">
														Número de Dispositivos
													</div>
												</div>
											</a>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<a href="marca.php" style="text-decoration: none;">
												<div class="card-body">
													<div class="row">
														<div class="col mt-0">
															<h5 class="card-title">Marca</h5>
														</div>

														<div class="col-auto">
															<div class="avatar">
																<div class="avatar-title rounded-circle bg-primary-dark">
																	<i class="align-middle" data-feather="list"></i>
																</div>
															</div>
														</div>
													</div>
													<h1 class="display-5 mt-2 mb-4">1</h1>
													<div class="mb-0">
														Número de marcas
													</div>
												</div>
											</a>
										</div>
										<div class="card">
											<a href="#" style="text-decoration: none;">
												<div class="card-body">
													<div class="row">
														<div class="col mt-0">
															<h5 class="card-title">Pedidos</h5>
														</div>

														<div class="col-auto">
															<div class="avatar">
																<div class="avatar-title rounded-circle bg-primary-dark">
																	<i class="align-middle" data-feather="truck"></i>
																</div>
															</div>
														</div>
													</div>
													<h1 class="display-5 mt-2 mb-4">0</h1>
													<div class="mb-0">
														Pedidos en proceso
													</div>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="col-xl-6 col-xxl-7 d-flex" style="height:275px;">
							<div class="card flex-fill w-100">
								<form id="smartwizard-validation" class="wizard wizard-primary" action="javascript:void(0)">
								<ul class="nav">
									<li class="nav-item"><a class="nav-link" href="#validation-step-1">Datos del Celular <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-2">Datos del Celular <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-3">Datos del Celular <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-4">Imagenes del Celular<br /><small></small></a></li>
								</ul>

								<div class="tab-content"> 
									<div id="validation-step-1" style="margin-bottom: 12px;" class="tab-pane" role="tabpanel">
										<div class="row">
										   <div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label ">Nombre
														<span class="text-danger">*</span>
													</label>
													<input id="nombre" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group mb-3"> 
													<label class="form-label">Precio
														<span class="text-danger">*</span>
													</label>
													<input id="precio" name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3" >
													<label class="form-label">Referencia
														<span class="text-danger">*</span>
													</label>
													<input id="referencia" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
										</div>
										<label class="form-label" style="margin-bottom: .3rem; margin-top: .2rem;">Descripción
											<span class="text-danger">*</span>
										</label>
										<div class="form-floating">
											<textarea id="descripcion" class="form-control" placeholder="Escribe una descripción" id="floatingTextarea1"
												style="height: 70px" required ></textarea>
											<label for="floatingTextarea1"></label>
										</div>
									</div>
									<div id="validation-step-2" style="max-height:230px; margin-bottom:1.5px;"class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label  for="inputState">Marca</label >
							                        <select id="inputState" name="marca" class="form-select flex-grow-1" style="width: 90%;" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM marca") as $row){ ?> 
							                            <option value="<?php echo $row['idMARCA'] ?>" ><?php echo $row['NOMBRE'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="" for="inputState">Categoria</label>
							                        <select id="inputState" name="categoria" class="form-select flex-grow-1" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM categoria") as $row){ ?> 
							                            <option value="<?php echo $row['ID'] ?>" ><?php echo $row['NOMBRE_CATEGORIA'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">												
												<div class="form-group "> 
													<label class="form-label">Red 3G/4G
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label ">SO
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label ">Color
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Procesador
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">#Nucleos
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">Memoria Interna
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group">
													<label class="form-label">Memoria RAM
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
										</div>
									</div>	

									<div id="validation-step-3"  style="max-height:230px; margin-bottom:1.5px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">												
													<div class="form-group  ">
													<label class="form-label">Camara Frontal
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">Camara Tracera
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											   <div class=" col-xl-4 ">												
													<div class="form-group">
													<label class="form-label">Peso
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">País
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>															
										   <div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">MemoriaEx
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group mb-3">
													<label class="form-label">Bateria
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">Duración Bateria
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Resolución
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">Dimensiones
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
										</div>
								    </div>	
									<div id="validation-step-4" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class="col-4">
												<div id="ImagenPreview" style="height:151px;">												
												</div>
												<div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 1
													  	</span>
											    		<input type="file" name="IMG1" id="imagen1" class="upload-box" placeholder="IMAGEN 1" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" required>
													</div>	
											</div>
											<div class="col-4">
												<div id="ImagenPreview2" style="height:151px; ">													
												</div>
												    <div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 2
													  	</span>
											    		<input type="file" name="IMG2" id="imagen2" class="upload-box" placeholder="IMAGEN 2" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff " >
													</div>	
											</div>
										</div>								
									</div>
								</div>
							</form>								
							</div>
						</div>
						<div class="col-xl-6 col-xxl-7 d-flex" style="height:275px; display:none !important">
							<div class="card flex-fill w-100">
								<form id="smartwizard-validation" class="wizard wizard-primary" action="javascript:void(0)">
								<ul class="nav">
									<li class="nav-item"><a class="nav-link" href="#validation-step-1">Datos del audifono  <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-2">Datos del audifono <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-3">Imagenes del audifono<br /><small></small></a></li>
								</ul>

								<div class="tab-content"> 
									<div id="validation-step-1" style="margin-bottom: 12px;" class="tab-pane" role="tabpanel">
										<div class="row">
										   <div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Nombre
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Referencia
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Precio
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>										
											</div>
										</div>
										<label class="form-label" style="margin-bottom: .3rem; margin-top: .2rem;">Descripción
											<span class="text-danger">*</span>
										</label>
										<div class="form-floating">
											<textarea class="form-control" placeholder="Escribe una descripción" id="floatingTextarea1"
												style="height: 70px" required ></textarea>
											<label for="floatingTextarea1"></label>
										</div>
									</div>
									<div id="validation-step-2" style="max-height:230px; margin-bottom:1.5px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label  for="inputState">Marca</label >
							                        <select id="inputState" name="marca" class="form-select flex-grow-1" style="width: 90%;" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM marca") as $row){ ?> 
							                            <option value="<?php echo $row['idMARCA'] ?>" ><?php echo $row['NOMBRE'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="" for="inputState">Categoria</label>
							                        <select id="inputState" name="categoria" class="form-select flex-grow-1" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM categoria") as $row){ ?> 
							                            <option value="<?php echo $row['ID'] ?>" ><?php echo $row['NOMBRE_CATEGORIA'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Conexión
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Modelo
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Entrada de audio
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Peso
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>											
											</div>
									            <div class=" col-xl-4 ">
												  <div class="form-group ">
													<label class="form-label">País 
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Manual
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group ">
													<label class="form-label">Garantia
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
										</div>
									</div>				
									<div id="validation-step-3" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class="col-4">
												<div id="ImagenPreview" style="height:151px;">												
												</div>
												<div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 1
													  	</span>
											    		<input type="file" name="IMG1" id="imagen1" class="upload-box" placeholder="IMAGEN 1" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" required>
													</div>	
											</div>
											<div class="col-4">
												<div id="ImagenPreview2" style="height:151px; ">		
												 </div>
												    <div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 2
													  	</span>
											    		<input type="file" name="IMG2" id="imagen2" class="upload-box" placeholder="IMAGEN 2" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff " >
											    	</div>	
											</div>
										</div>									
									</div>
								</div>
							</form>								
					     	</div>
						</div>	-->		
						<div class="col-xl-6 col-xxl-7 d-flex" style="height:275px;">
							<div  class="card flex-fill w-100">
								<form id="smartwizard-validation" method="POST" class="wizard wizard-primary" >
								<ul class="nav">
									<li class="nav-item"><a class="nav-link" href="#validation-step-1">Datos del PC <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-2">Datos del PC <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-3">Imagenes del PC
										<br /><small></small></a></li>
								</ul>
								<div class="tab-content"> 
									<div id="validation-step-1" style="margin-bottom: 33px;" class="tab-pane" role="tabpanel">
									  <div class="row">
										   <div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Nombre
														<span class="text-danger">*</span>
													</label>
													<input id="nombre" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Precio
														<span class="text-danger">*</span>
													</label>
													<input id="precio" name="Precio" type="number" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Referencia
														<span class="text-danger">*</span>
													</label>
													<input id="referencia" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
									  </div>
										<label class="form-label" style="margin-bottom: .3rem; margin-top: .2rem;">Descripción
											<span class="text-danger">*</span>
										</label>
										<div class="form-floating">
											<textarea class="form-control" placeholder="Escribe una descripción" id="floatingTextarea1"
												style="height: 70px" required ></textarea>
											<label for="floatingTextarea1"></label>
										</div>
									</div>											
									<div id="validation-step-2" style="max-height:230px; margin-bottom:22px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label  for="inputState">Marca</label >
							                        <select id="inputState" name="marca" class="form-select flex-grow-1" style="width: 90%;" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM marca") as $row){ ?> 
							                            <option value="<?php echo $row['idMARCA'] ?>" ><?php echo $row['NOMBRE'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="" for="inputState">Categoria</label>
							                        <select id="inputState2" name="categoria" class="form-select flex-grow-1" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM categoria") as $row){ ?> 
							                            <option value="<?php echo $row['ID'] ?>" ><?php echo $row['NOMBRE_CATEGORIA'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>											
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label ">Color
														<span class="text-danger">*</span>
													</label>
													<input id="color" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">						
												<div class="form-group ">
													<label class="form-label">Cantidad
														<span class="text-danger">*</span>
													</label>
													<input id="cantidad" name="Precio" type="number" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label ">SO
														<span class="text-danger">*</span>
													</label>
													<input id="so" name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">										
												<div class="form-group ">
													<label class="form-label">RAM
														<span class="text-danger">*</span>
													</label>
													<input id="ram" name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">										
												<div class="form-group ">
													<label class="form-label">Disco Duro
														<span class="text-danger">*</span>
													</label>
													<input id="disco" name="Precio" type="number" class="form-control required">
												</div>												
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Garantia
														<span class="text-danger">*</span>
													</label>
													<input id="garantia" name="Precio" type="number" class="form-control required">
												</div>											
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Estado
														<span class="text-danger">*</span>
													</label>
													<input id="estado" name="Precio" type="number" class="form-control required">
												</div>											
											</div>
										</div>								
									</div>
									<div id="validation-step-3" style="margin-bottom: 23px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class="col-4">
												<div id="ImagenPreview" style="height:151px;">												
												</div>
												<div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 1
													  	</span>
											    		<input  type="file" name="IMG1" id="imagen1" class="upload-box" placeholder="IMAGEN 1" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" required>
													</div>	
											</div>
											<div class="col-4">
												<div id="ImagenPreview2" style="height:151px; ">		
												 </div>
												    <div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 2
													  	</span>
											    		<input type="file" name="IMG2" id="imagen2" class="upload-box" placeholder="IMAGEN 2" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff " >
											    	</div>	
											</div>
										</div>									
									</div>
								</div>							
						    	</form>
							</div>
						</div>
						<div class="col-xl-6 col-xxl-7 d-flex" style="height:275px; display:none !important ">
							<div class="card flex-fill w-100">
								<form id="smartwizard-validation" class="wizard wizard-primary" action="javascript:void(0)">
								<ul class="nav">
								<li class="nav-item"><a class="nav-link" href="#validation-step-1">Datos de Smartwatch  <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-2">Datos de Smartwatch <br /><small></small></a></li>
									<li class="nav-item"><a class="nav-link" href="#validation-step-3">Imagenes<br /><small></small></a></li>
								</ul>
								<div class="tab-content"> 
									<div id="validation-step-1" style="margin-bottom: 32px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Nombre
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Referencia
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Precio
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>										
											</div>
										</div>
										<label class="form-label" style="margin-bottom: .3rem; margin-top: .2rem;">Descripción
											<span class="text-danger">*</span>
										</label>
										<div class="form-floating">
											<textarea class="form-control" placeholder="Escribe una descripción" id="floatingTextarea1"
												style="height: 70px" required ></textarea>
											<label for="floatingTextarea1"></label>
										</div>
									</div>
									<div id="validation-step-2" style="max-height:230px; margin-bottom:22px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label  for="inputState">Marca</label >
							                        <select id="inputState" name="marca" class="form-select flex-grow-1" style="width: 90%;" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM marca") as $row){ ?> 
							                            <option value="<?php echo $row['idMARCA'] ?>" ><?php echo $row['NOMBRE'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="" for="inputState">Categoria</label>
							                        <select id="inputState" name="categoria" class="form-select flex-grow-1" required>
							                            <option value="" >Elige una opción</option>
							                            <?php foreach ($conexion->query("SELECT * FROM categoria") as $row){ ?> 
							                            <option value="<?php echo $row['ID'] ?>" ><?php echo $row['NOMBRE_CATEGORIA'] ?></option>
							                            <?php
							                                }
							                            ?>        
							                        </select>
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3 ">
													<label class="form-label">Conexión
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Compatibilidad
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Dimension AxA
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
											<div class=" col-xl-4 ">
												<div class="form-group mb-3">
													<label class="form-label">Dimensiones A
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
									     	<div class=" col-xl-4 ">
												<div class="form-group ">
													<label class="form-label">Dimension P
														<span class="text-danger">*</span>
													</label>
													<input name="wizard-userName" type="text" class="form-control required">
												</div>
											</div>
										    <div class=" col-xl-4 ">
												<div class="form-group m">
													<label class="form-label">Tamaño Pantalla
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>											
											</div>
											<div class=" col-xl-4 ">												
													<div class="form-group mb-3">
													<label class="form-label">Garantia
														<span class="text-danger">*</span>
													</label>
													<input name="Precio" type="number" class="form-control required">
												</div>												
											</div>
										</div>
									</div>
									<div id="validation-step-3" style="margin-bottom: 23px;" class="tab-pane" role="tabpanel">
										<div class="row">
											<div class="col-4">
												<div id="ImagenPreview" style="height:151px;">												
												</div>
												<div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 1
													  	</span>
											    		<input type="file" name="IMG1" id="imagen1" class="upload-box" placeholder="IMAGEN 1" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff" required>
													</div>	
											</div>
											<div class="col-4">
												<div id="ImagenPreview2" style="height:151px; ">		
												 </div>
												    <div class="button-wrapper">
													  	<span class="label">
													    IMAGEN 2
													  	</span>
											    		<input type="file" name="IMG2" id="imagen2" class="upload-box" placeholder="IMAGEN 2" accept="application/pdf,image/x-png,image/gif,image/jpeg,image/jpg,image/tiff " >
											    	</div>	
											</div>
											<div id="respuesta" class="alert alert-success">
											</div
										</div>									
									</div>
								</div>
							</form>								
					     	</div>
						</div>	
					</div>		
					<div class="row">
						<div class="col-xxl-9">
							<div class="card">
								<div class="card-header">
									<div class="card-actions float-end">
										<a href="#" class="me-1">
											<i class="align-middle" data-feather="refresh-cw"></i>
										</a>
										
									</div>
									<h5 class="card-title mb-0">Clients</h5>
								</div>
								<div class="card-body">
									<table id="datatables-clients" class="table table-striped" style="width:100%">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Company</th>
												<th>Email</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Garrett Winters</td>
												<td>Good Guys</td>
												<td>garrett@winters.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Ashton Cox</td>
												<td>Levitz Furniture</td>
												<td>ashton@cox.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Sonya Frost</td>
												<td>Child World</td>
												<td>sonya@frost.com</td>
												<td><span class="badge bg-danger">Deleted</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Jena Gaines</td>
												<td>Helping Hand</td>
												<td>jena@gaines.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Quinn Flynn</td>
												<td>Good Guys</td>
												<td>quinn@flynn.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Charde Marshall</td>
												<td>Price Savers</td>
												<td>charde@marshall.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Haley Kennedy</td>
												<td>Helping Hand</td>
												<td>haley@kennedy.com</td>
												<td><span class="badge bg-danger">Deleted</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Tatyana Fitzpatrick</td>
												<td>Good Guys</td>
												<td>tatyana@fitzpatrick.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Michael Silva</td>
												<td>Red Robin Stores</td>
												<td>michael@silva.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Yuri Berry</td>
												<td>The Wiz</td>
												<td>yuri@berry.com</td>
												<td><span class="badge bg-danger">Deleted</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Doris Wilder</td>
												<td>Red Robin Stores</td>
												<td>doris@wilder.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Angelica Ramos</td>
												<td>The Wiz</td>
												<td>angelica@ramos.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Jennifer Chang</td>
												<td>Helping Hand</td>
												<td>jennifer@chang.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Brenden Wagner</td>
												<td>The Wiz</td>
												<td>brenden@wagner.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Fiona Green</td>
												<td>The Sample</td>
												<td>fiona@green.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Suki Burks</td>
												<td>The Sample</td>
												<td>suki@burks.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Prescott Bartlett</td>
												<td>The Sample</td>
												<td>prescott@bartlett.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Gavin Cortez</td>
												<td>Red Robin Stores</td>
												<td>gavin@cortez.com</td>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Unity Butler</td>
												<td>Price Savers</td>
												<td>unity@butler.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
											<tr>
												<td><img src="recursos/img/avatars/user.jpg" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
												<td>Howard Hatfield</td>
												<td>Price Savers</td>
												<td>howard@hatfield.com</td>
												<td><span class="badge bg-warning">Inactive</span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-xxl-3">
							<div class="card">
								<div class="card-header">
									<div class="card-actions float-end">
										<a href="#" class="me-1">
											<i class="align-middle" data-feather="refresh-cw"></i>
										</a>
										<div class="d-inline-block dropdown show">
											<a href="#" data-bs-toggle="dropdown" data-bs-display="static">
												<i class="align-middle" data-feather="more-vertical"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-end">
												<a class="dropdown-item" href="#">Descargar</a>
											</div>
										</div>
									</div>
									<h5 class="card-title mb-0">Angelica Ramos</h5>
								</div>
								<div class="card-body">
									<div class="row g-0">
										<div class="col-sm-3 col-xl-12 col-xxl-4 text-center">
											<img src="recursos/img/avatars/user.jpg" width="64" height="64" class="rounded-circle mt-2" alt="Angelica Ramos">
										</div>
										<div class="col-sm-9 col-xl-12 col-xxl-8">
											<strong>About me</strong>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
												sociis
												natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
										</div>
									</div>

									<table class="table table-sm my-2">
										<tbody>
											<tr>
												<th>Name</th>
												<td>Charissa Hilt</td>
											</tr>
											<tr>
												<th>Company</th>
												<td>Matrix Interior Design</td>
											</tr>
											<tr>
												<th>Occupation</th>
												<td>Desktop publisher</td>
											</tr>
											<tr>
												<th>Email</th>
												<td>charissahilt@rhyta.com</td>
											</tr>
											<tr>
												<th>Phone</th>
												<td>+1234123123123</td>
											</tr>
											<tr>
												<th>Website</th>
												<td>hispanomarketer.com</td>
											</tr>
											<tr>
												<th>Status</th>
												<td><span class="badge bg-success">Active</span></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
	<?php include 'recursos/php/dashboard/pie.php'; ?>
	<script text="text/javascript">			
		$(document).ready(function() {
   		 const $nombre = $("#nombre"),
        $precio      = $("#precio"),
        $referencia  = $("#referencia"),
		$descripcion = $("#floatingTextarea1"),
		$marca       = $("#inputState"),
		$categoria   = $("#inputState2"),
		$color 	     = $("#color"),
		$cantidad    = $("#cantidad	"),
		$so          = $("#so"),
		$ram 		 = $("#ram"),
		$disco 		 = $("#disco"),
		$garantia    = $("#garantia"),
		$estado      = $("#estado"),
		$imagen1     = $("#imagen1"),
		$imagen2	 = $("#imagen2"),
        $botonenviar = $("#botonenviar"),
        $respuesta = $("#respuesta");

  	    $botonenviar.click(function() {

        $.post("enviar.php", {
            nombre: $nombre.val(),
            precio: $precio.val(),
            referencia: $referencia.val(),
			floatingTextarea1: $descripcion.val()

        })
		$('#smartwizard-validation').trigger('reset');
    });

});														
	</script>


	<script>
		document.addEventListener("DOMContentLoaded", function() {
			// Datatables clients
			$("#datatables-clients").DataTable({
				responsive: true,
				order: [
					[1, "asc"]
				]
			});
		});
	</script>
	<script>
		document.addEventListener("DOMContentLoaded", function() {
			$("#smartwizard-default-primary").smartWizard({
				theme: "default",
				showStepURLhash: false
			});
			$("#smartwizard-default-success").smartWizard({
				theme: "default",
				showStepURLhash: false
			});
			$("#smartwizard-default-danger").smartWizard({
				theme: "default",
				showStepURLhash: false
			});
			$("#smartwizard-default-warning").smartWizard({
				theme: "default",
				showStepURLhash: false
			});
			$("#smartwizard-arrows-primary").smartWizard({
				theme: "arrows",
				showStepURLhash: false
			});
			$("#smartwizard-arrows-success").smartWizard({
				theme: "arrows",
				showStepURLhash: false
			});
			$("#smartwizard-arrows-danger").smartWizard({
				theme: "arrows",
				showStepURLhash: false
			});
			$("#smartwizard-arrows-warning").smartWizard({
				theme: "arrows",
				showStepURLhash: false
			});
			// Validation
			var $validationForm = $("#smartwizard-validation");
			$validationForm.validate({
				errorPlacement: function errorPlacement(error, element) {
					$(element).parents(".error-placeholder").append(
						error.addClass("invalid-feedback small d-block")
					)
				},
				highlight: function(element) {
					$(element).addClass("is-invalid");
				},
				unhighlight: function(element) {
					$(element).removeClass("is-invalid");
				},
				rules: {
					"wizard-confirm": {
						equalTo: "input[name=\"wizard-password\"]"
					}
				}
			 });
			 $validationForm
				.smartWizard({
					autoAdjustHeight: false,
					backButtonSupport: false,
					useURLhash: false,
					showStepURLhash: false,
					toolbarSettings: {
						toolbarExtraButtons: [$("<button class=\"btn btn-submit btn-primary\" id=\"botonenviar\" type=\"submit\" value=\"enviar\">Guardar</button>")]
					}
				})
				.on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
					if (stepDirection === 1) {
						return $validationForm.valid();
					}
					return true;
				});
			$validationForm.find(".btn-submit").on("click", function() {
				if (!$validationForm.valid()) {
					return;
				}
				alert("Información agregada correctamente!");
				return false;
			});
		});
	</script>
	<script type="text/javascript">
	(function(){
    function filePreview(input){
	    if(input.files && input.files[0]){
	        var reader = new FileReader();

	        reader.onload = function(e){
	            $('#ImagenPreview').html("<center><img style='width: 142px; height:151px'; src='"+e.target.result+"' /> </center>");
	        }

	        reader.readAsDataURL(input.files[0]);
	    	}
		}

		$('#imagen1').change(function(){
			filePreview(this);
		});

	})();
	(function(){
    function filePreview(input){
	    if(input.files && input.files[0]){
	        var reader = new FileReader();

	        reader.onload = function(e){
	            $('#ImagenPreview2').html("<center> <img style='width: 142px; height:151px'; src='"+e.target.result+"' /> </center>");
	        }

	        reader.readAsDataURL(input.files[0]);
	    	}
		}

		$('#imagen2').change(function(){
			filePreview(this);
		});

	})();
	(function(){
    function filePreview(input){
	    if(input.files && input.files[0]){
	        var reader = new FileReader();

	        reader.onload = function(e){
	            $('#ImagenPreview3').html("<center><img style='width: 142px; height:151px'; src='"+e.target.result+"' /></center>");
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

		$('#imagen3').change(function(){
			filePreview(this);
		});

	})();
	</script>
</html>


