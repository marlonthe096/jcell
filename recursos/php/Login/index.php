<!DOCTYPE html>
<html lang="en">
<head>
	
   

	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../../img/Loginfavicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../loginr/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../loginr/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../loginr/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../loginr/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../css/logincss2.css">
	<link rel="stylesheet" type="text/css" href="../../css/sweetalert.css">
<!--===============================================================================================-->
</head>

	<?php session_start();

	    if (isset($_SESSION['tipo'])) {

	    	 header("location: ../../../index.php");

	    }else {
	       
	    }
	    
   ?>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="../../img/imgLogin.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="login.php" method="POST">
					<span class="login100-form-title">
						Iniciar sesión para continuar
					</span>

					<div class="wrap-input100 validate-input" data-validate = "La identificacion es requerida">
						<input class="input100" type="text" name="user" placeholder="Identificación">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-id-card-o" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "La contraseña es requerida">
						<input class="input100" type="password" name="password" placeholder="Contraseña">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Entrar
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Olvidar
						</span>
						<a class="txt2" href="#">
							Usuario / Contraseña?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							Solicitar un usuario
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->	
	<script src="../../loginr/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../../loginr/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../../loginr/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../../loginr/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="../../js/loginjs.js"></script>
	<script src="../../js/functions.js" type="text/javascript"></script>
	<script src="../../js/sweetalert.min.js" type="text/javascript"></script>

	<?php if (isset($_SESSION['mensaje'])) { ?>
		<script type="text/javascript">
			swal("Error","", "warning");
		</script>
	<?php $_SESSION['mensaje'] = null;} ?>
	

</body>
</html>